# Droplet

Droplet is *free software* that solves the 3D *coupled* convecto - conducto
*thermal problem* of a droplet on a planar base. This C89
program internally relies on *Monte-Carlo* algorithms based on reformulations
of the main heat transfer phenomena as cross-recursive "thermal paths" that
explore space until a boundary condition is
found. The key concept here is that heat transfer phenomena are not considered
separately but naturally coupled via cross-recursive [Monte-Carlo
algorithms](https://hal.archives-ouvertes.fr/hal-02419604/).

The hypothesis these algorithms are based upon are the following:

- *conduction*: thermal heat transfer in solids uses the walk-on-sphere
  approach. Solutions obtained using this algorithm are formally exact for
  stationary computations.
- *convection*: fluid media are supposed to be isothermal.
  This hypothesis relies on the assumption of perfectly agitated fluids.
- *solid/fluid interface*: depending on parameters, paths can bounce back at
  the solid/fluid interface. This introduces the need for a reinjection 
  distance within the Monte-Carlo algorithm. Solutions obtained using this
  algorithm are formally exact at the limit of a null reinjection distance.
  In practice, this distance has to be adapted so that its value comply
  with the temperature gradient requirements.

## How to build

Droplet is compatible GNU/Linux as well as Microsoft Windows 7 and
later, both in 64-bits. It was successfully built with the [GNU Compiler
Collection](https://gcc.gnu.org) (versions 4.9.2 and later) as well as with
Microsoft Visual Studio 2019.

Droplet relies on the [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) package to build.
It also depends on the
[RSys](https://gitlab.com/vaplv/rsys/),
[Star-SP](https://gitlab.com/meso-star/star-sp/) libraries as well as on the
[OpenMP](http://www.openmp.org) 2.0 specification to parallelize its
computations. On Windows systems it additionally depends on
[MuslGetopt](https://gitlab.com/meso-star/musl-getopt/).

First ensure that CMake and a C compiler that implements the OpenMP 2.0
specification are installed on your system. Then install the RCMake package as
well as all the aforementioned prerequisites. Finally generate the project from
the `cmake/CMakeLists.txt` file by appending to the `CMAKE_PREFIX_PATH`
variable the install directories of its dependencies.

The easiest way to have Droplet installed on your system is by using
the droplet branch of the star-engine. Please refer to the
[dedicated help](https://gitlab.com/meso-star/star-engine/-/tree/droplet).

## Release notes

### Version 1.0

Computation of the total flux on the solid-fluid interface.

Computation of the mean temperature on the solid-fluid interface.

### Version 0.0

Computation of the steady temperature at a probe position.

## License

Copyright (C) 2021 |Meso|Star> (<contact@meso-star.com>). Droplet
is free software released under the
[GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html) license: GNU GPL version 3
or later.
You are welcome to redistribute it under certain conditions; refer to the
COPYING files for details.

