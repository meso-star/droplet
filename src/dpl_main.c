/* Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "droplet.h"
#include "dpl_system.h"
#include "dpl_solver.h"
#include "dpl_parsing.h"

#include <rsys/double3.h>

#include <rsys/rsys.h>
#include <rsys/clock_time.h>

#include <stdio.h>

int
main
  (int argc, char* argv[])
{
  res_T res = RES_OK;
  struct system system;
  struct estimator result = ESTIMATOR_DEFAULT;
  struct mc temp, flux, steps, reinjections, air_vs_base;
  struct time t0, t1;
  char tdump[128];
  double refT = 0;
  double refF = 0;
  double Tf = -1;

  time_current(&t0);

  /* Init system */
  ERR(init_system(&system));
  ERR(set_system_physics(1, 0, 1, &system));

  /* Parse options */
  ERR(parse_args(argc, argv, &system));

  /* Init probe */
  if(system.compute_mode == COMPUTE_PROBE) {
    ERR(define_probe(&system));
  }

  if(!system.compact_result) {
    /* Print description */
    if(system.probe_location == PROBE_ON_BOUNDARY)
      Tf = fluid_T(&system, system.solver_probe);
    printf("----------------------\n");
    printf("Running on %u threads.\n", system.nthreads);

    printf("System description:\n");
    printf("  Base offset: %g\n", system.offset);
    printf("  Radius: %g\n", system.radius);
    printf("  Contact angle: %g degrees\n", system.contact_angle);
    printf("  Hc: %g\n", system.hc);
    printf("  Tair: %g\n", system.Tair);
    printf("  Tbase: %g\n", system.Tbase);
    printf("  lambda: %g\n", system.lambda);

    printf("Numerical parameters:\n");
    printf("  Boundary epsilon: %g * radius = %g\n",
      system.boundary_epsilon, system.boundary_epsilon * system.radius);
    printf("  Reinjection length: %g * epsilon = %g\n",
      system.reinjection_length,
      system.reinjection_length * system.boundary_epsilon * system.radius);

    if(system.compute_mode == COMPUTE_PROBE) {
      if(system.probe_location == PROBE_ON_BOUNDARY) {
        printf("\nProbe position at boundary: (%.9g, %.9g, %.9g), Tfluid = %g\n",
          SPLIT3(system.user_probe), Tf);
      } else {
        printf("\nProbe position: (%.9g, %.9g, %.9g)\n",
          SPLIT3(system.user_probe));
      }
    } else {
      printf("\nComputing mean values on the interface.\n");
      if(system.compute_range[1] != system.radius)
        printf("Compute zone: Z in [%.9g %.9g].\n",
          system.offset, system.compute_range[1]);
    }

    switch(system.sampling_mode) {
      case SAMPLE_UNIFORM:
        printf("Uniform sampling.\n");
        break;
      default: FATAL("Invalid type.\n");
    }


    if(system.solver_mode & ANALYTIC_SPHERE) {
      refT = analytic_T(&system, system.solver_probe);
      printf("\nSolver on analytic mode.\n");
      printf("  X1: %g\n", system.x1);
      printf("\nExpected T: %g\n", refT);
      if(system.probe_location == PROBE_ON_BOUNDARY) {
        refF = (Tf - refT) * system.hc;
        printf("Expected Flux density: %g\n", refF);
      }
    }
  }

  /* Solve */
  res = solve(&system, &result);
  if(res != RES_OK)  {
    fprintf(stderr, "\nSimulation failed!\n");
    goto error;
  }

  time_sub(&t0, time_current(&t1), &t0);
  if(!system.compact_result) {
    /* Print computation time */
    time_dump(&t0, TIME_ALL, NULL, tdump, sizeof(tdump));
    printf("\nElapsed time = %s.\n", tdump);
  }

  /* Print results */
  SETUP_MC(&temp, &result.temperature);
  SETUP_MC(&flux, &result.flux);
  SETUP_MC(&steps, &result.steps);
  SETUP_MC(&reinjections, &result.reinjections);
  SETUP_MC(&air_vs_base, &result.air_vs_base);
  if(!system.compact_result) {
    printf("%lu samples, %lu failures.\n",
      (unsigned long)result.n_realisations, (unsigned long)result.n_failures);

    if((system.solver_mode & ANALYTIC_SPHERE) || system.ref_t) {
      if(system.ref_t) refT = system.reference_temperature;
      printf("\nTemperature: %g +/- %g [%g sigma, %.2g %% VS ref].\n",
        SPLITMC(temp), (temp.E - refT) / temp.SE, 100 * (temp.E - refT) / refT);
    }
    if((system.solver_mode & ANALYTIC_SPHERE) || system.ref_f) {
      if(system.ref_f) refF = system.reference_flux;
      printf("Flux%s: %g +/- %g [%g sigma, %.2g %% VS ref].\n",
        (system.compute_mode == COMPUTE_PROBE ? " density" : ""),
        SPLITMC(flux), (flux.E - refF) / flux.SE, 100 * (flux.E - refF) / refF);
    } else {
      printf("\nTemperature: %g +/- %g [%g %g].\n",
        SPLITMC(temp), temp.E - 3 * temp.SE, temp.E + 3 * temp.SE);
      if(system.probe_location == PROBE_ON_BOUNDARY)
        printf("Flux%s: %g +/- %g [%g %g].\n",
          (system.compute_mode == COMPUTE_PROBE ? " density" : ""),
          SPLITMC(flux), flux.E - 3 * flux.SE, flux.E + 3 * flux.SE);
    }
    printf("Steps per sample: %g +/- %g\n", SPLITMC(steps));
    printf("Reinjections per sample: %g +/- %g\n", SPLITMC(reinjections));
    printf("Air VS base samplings per sample: %g +/- %g\n", SPLITMC(air_vs_base));
  } else {
    /* Print compact results */
    double t = (double)time_val(&t0, TIME_NSEC) * 1e-9;
    printf("%.9g %.9g %.9g %.9g %.9g %g %lu %lu %.9g %.9g %.9g %.9g %.9g %.9g",
      SPLIT3(system.user_probe), system.contact_angle, system.hc, t,
      (unsigned long)result.n_realisations, (unsigned long)result.n_failures,
      system.boundary_epsilon, system.reinjection_length,
      SPLITMC(temp), SPLITMC(flux));
    if(system.ref_t) {
      refT = system.reference_temperature;
      printf(" %.9g", 100 * (temp.E - refT) / refT);
    }
    if(system.ref_f) {
      refF = system.reference_flux;
      printf(" %.9g", 100 * (flux.E - refF) / refF);
    }
    printf("\n");
  }

end:
  clear_system(&system);
  return (res == RES_OK) ? EXIT_SUCCESS : EXIT_FAILURE;

error:
  goto end;
}
