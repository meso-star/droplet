/* Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "droplet.h"
#include "dpl_parsing.h"
#include "dpl_num_params.h"
#include "dpl_system.h"
#include "dpl_walk.h"

#include <rsys/cstr.h>
#include <rsys/double3.h>

#include <math.h>
#include <getopt.h>
#include <string.h>
#include <omp.h>

void
short_help
  (FILE* stream)
{
  fprintf(stream, "Usage: droplet [OPTIONS]\n");
  fprintf(stream, "  Solve the droplet boundary temperature.\n");

  fprintf(stream, "Options:\n");

  fprintf(stream, "-a ANGLE\n");
  fprintf(stream, "  Set the contact angle (in ]0 180[ degrees.\n");

  fprintf(stream, "-A X1\n");
  fprintf(stream, "  Switch to analytic droplet mode.\n");
  fprintf(stream, "  X1 is the origin of the analytic temperature field.\n");

  fprintf(stream, "-c\n");
  fprintf(stream, "  Print compact results.\n");


  fprintf(stream, "-E EPS\n");
  fprintf(stream,
    "  Set the BOUNDARY_EPSILON numerical parameter (in ]0 inf), default %g).\n",
    (double)BOUNDARY_EPSILON);

  fprintf(stream, "-h HC\n");
  fprintf(stream, "  Set the convection coefficient at the boundary.\n");

  fprintf(stream, "-i\n");
  fprintf(stream, "  Compute the mean temperature and flux over the whole boundary.\n");

  fprintf(stream, "-I\n");
  fprintf(stream, "  Compute mean values on a fraction of the droplet.\n");
  fprintf(stream, "  Valid range ]BASE_OFFSET 1[\n");

  fprintf(stream, "-L LENGTH\n");
  fprintf(stream,
    "  Set the REINJECTION_LENGTH numerical parameter (in ]1 inf), default %g).\n",
    (double)REINJECTION_LENGTH);

  fprintf(stream, "-p X,Y,Z\n");
  fprintf(stream,
    "  Compute the temperature and flux density at a probe point into the droplet.\n");

  fprintf(stream, "-P HEIGHT\n");
  fprintf(stream,
    "  Compute the temperature and flux density at a probe point on the boundary.\n");
  fprintf(stream, "  Valid range ]0 RADIUS-BASE_OFFSET[\n");

  fprintf(stream, "-r <Temp reference>\n");
  fprintf(stream, "  Give an user reference for the temperature.\n");
  fprintf(stream, "  If provided, difference in %% to the reference is computed.\n");

  fprintf(stream, "-R <Flux reference>\n");
  fprintf(stream, "  Give an user reference for the flux (density).\n");
  fprintf(stream, "  If provided, difference in %% to the reference is computed.\n");

  fprintf(stream, "-s COUNT\n");
  fprintf(stream, "  Set the sample count (default 10000).\n");

  fprintf(stream, "-S SAMPLING_MODE\n");
  fprintf(stream, "  Set sampling mode: u for uniform (default).\n");

  fprintf(stream, "-t COUNT\n");
  fprintf(stream,
    "  Set the thread count (default %d).\n", omp_get_num_procs());
}

res_T
parse_args
  (int argc,
   char** argv,
   struct system* system)
{
  res_T res = RES_OK;
  const char option_list[] = "a:A:cE:h:iI:L:p:P:r:R:s:S:t:";
  int opt;
  int provided_a = 0, provided_A = 0, provided_h = 0, provided_I = 0,
    provided_i = 0, provided_P = 0, provided_S = 0;
  int help = 0;
  size_t len;
  double max_range = system->radius;

  ASSERT(argc >= 1 && argv && system);

  opterr = 0; /* No default error messages */
  while((opt = getopt(argc, argv, option_list)) != -1) {
    switch(opt) {

      case '?': /* Unknown option */
      {
        char* ptr = strchr(option_list, optopt);
        if(ptr && ptr[1] == ':') {
          fprintf(stderr, "Missing argument for option -%c.\n", optopt);
        } else {
          fprintf(stderr, "Unknown option: -%c\n", optopt);
        }
        res = RES_BAD_ARG;
        goto error;
      }

      case 'a':
        res = cstr_to_double(optarg, &system->contact_angle);
        if(res != RES_OK
          || system->contact_angle <= 0 || system->contact_angle >= 180)
        {
          if(res == RES_OK) res = RES_BAD_ARG;
          fprintf(stderr,
            "Argument for option -%c is out of range: %g.\n",
             opt, system->contact_angle);
          goto error;
        }
        provided_a = 1;
        break;

      case 'A': {
        if(provided_i) {
          fprintf(stderr, "Cannot use both -i and -A.\n");
          res = RES_BAD_ARG;
          goto error;
        }
        system->solver_mode = ANALYTIC_SPHERE;
        /* Parse X1 */
        res = cstr_to_double(optarg, &system->x1);
        if(res != RES_OK
          || system->x1 <= 0)
        {
          fprintf(stderr, "Invalid argument for option -%c: %s.\n", opt, optarg);
          res = RES_BAD_ARG;
          goto error;
        }
        provided_A = 1;
        break;
      }

      case 'c':
        system->compact_result = 1;
        break;

      case 'E':
        res = cstr_to_double(optarg, &system->boundary_epsilon);
        if(res != RES_OK
          || system->boundary_epsilon <= 0)
        {
          if(res == RES_OK) res = RES_BAD_ARG;
          fprintf(stderr, "Invalid argument for option -%c: %s.\n", opt, optarg);
          goto error;
        }
        break;

      case 'h':
        res = cstr_to_double(optarg, &system->hc);
        if(res != RES_OK
          || system->hc <= 0)
        {
          if(res == RES_OK) res = RES_BAD_ARG;
          fprintf(stderr, "Invalid argument for option -%c: %s.\n", opt, optarg);
          goto error;
        }
        provided_h = 1;
        break;

      case 'i':
        if(provided_A) {
          fprintf(stderr, "Cannot use both -i and -A.\n");
          res = RES_BAD_ARG;
          goto error;
        }
        if(provided_P) {
          fprintf(stderr, "Cannot use both -i and -p | -P.\n");
          res = RES_BAD_ARG;
          goto error;
        }
        provided_i = 1;
        system->compute_mode = COMPUTE_MEANS;
        system->probe_location = PROBE_ON_BOUNDARY;
        break;

      case 'I':
        if(provided_P) {
          fprintf(stderr, "Cannot use both -i and -p | -P.\n");
          res = RES_BAD_ARG;
          goto error;
        }
        res = cstr_to_double(optarg, &max_range);
        if(res != RES_OK) {
          fprintf(stderr, "Invalid argument for option -%c: %s.\n", opt, optarg);
          goto error;
        }
        /* max_range cannot be checked until parsing complete */
        provided_I = 1;
        break;

      case 'L':
        res = cstr_to_double(optarg, &system->reinjection_length);
        if(res != RES_OK
          || system->reinjection_length <= 1)
        {
          if(res == RES_OK) res = RES_BAD_ARG;
          fprintf(stderr, "Invalid argument for option -%c: %s.\n", opt, optarg);
          goto error;
        }
        break;

      case 'p':
        if(provided_i) {
          fprintf(stderr, "Cannot use both -i and -p | -P.\n");
          res = RES_BAD_ARG;
          goto error;
        }
        res = cstr_to_list_double(optarg, ',', system->args_probe, &len, 3);
        if(res != RES_OK || len != 3) {
          if(res == RES_OK) res = RES_BAD_ARG;
          /* Cannot check value until geometry is known */
          fprintf(stderr, "Invalid argument for option -%c: %s.\n", opt, optarg);
          goto error;
        }
        provided_P = 1;
        system->compute_mode = COMPUTE_PROBE;
        system->probe_location = PROBE_IN_DROPLET;
        break;

      case 'P':
        if(provided_i) {
          fprintf(stderr, "Cannot use both -i and -p | -P.\n");
          res = RES_BAD_ARG;
          goto error;
        }
        res = cstr_to_double(optarg, &system->args_probe[2]);
        if(res != RES_OK) {
          /* Cannot check value until geometry is known */
          fprintf(stderr, "Invalid argument for option -%c: %s.\n", opt, optarg);
          goto error;
        }
        provided_P = 1;
        system->compute_mode = COMPUTE_PROBE;
        system->probe_location = PROBE_ON_BOUNDARY;
        break;

      case 'r':
        res = cstr_to_double(optarg, &system->reference_temperature);
        if(res != RES_OK
          || system->reference_temperature < 0)
        {
          if(res == RES_OK) res = RES_BAD_ARG;
          fprintf(stderr, "Invalid argument for option -%c: %s.\n", opt, optarg);
          goto error;
        }
        system->ref_t = 1;
        break;

      case 'R':
        res = cstr_to_double(optarg, &system->reference_flux);
        if(res != RES_OK) {
          if(res == RES_OK) res = RES_BAD_ARG;
          fprintf(stderr, "Invalid argument for option -%c: %s.\n", opt, optarg);
          goto error;
        }
        system->ref_f = 1;
        break;

      case 's':
        res = cstr_to_ulong(optarg, &system->sample_count);
        if(res != RES_OK
          || system->sample_count <= 0)
        {
          if(res == RES_OK) res = RES_BAD_ARG;
          fprintf(stderr, "Invalid argument for option -%c: %s.\n", opt, optarg);
          goto error;
        }
        break;

      case 'S':
        if(0 == strcmp("u", optarg))
          system->sampling_mode = SAMPLE_UNIFORM;
        else {
          res = RES_BAD_ARG;
          fprintf(stderr, "Invalid argument for option -%c: %s.\n", opt, optarg);
          goto error;
        }
        provided_S = 1;
        break;

      case 't':
        res = cstr_to_uint(optarg, &system->nthreads);
        if(res != RES_OK
          || system->nthreads <= 0)
        {
          if(res == RES_OK) res = RES_BAD_ARG;
          fprintf(stderr, "Invalid argument for option -%c: %s.\n", opt, optarg);
          goto error;
        }
        break;
    }
  }

  /* Check mandatory options */
  if(!provided_h) {
    fprintf(stderr, "Missing mandatory option -h.\n");
    res = RES_BAD_ARG;
    goto error;
  }
  if(!provided_a) {
    fprintf(stderr, "Missing mandatory option -a.\n");
    res = RES_BAD_ARG;
    goto error;
  }
  if(!provided_P && !provided_i) {
    fprintf(stderr, "Missing mandatory option -i | -p | -P.\n");
    res = RES_BAD_ARG;
    goto error;
  }
  if(provided_I && !provided_i) {
    fprintf(stderr, "Cannot use option -I whitout option -i.\n");
    res = RES_BAD_ARG;
    goto error;
  }

  /* check for unused arguments */
  if(optind < argc) {
    int i;
    fprintf(stderr, "Unused command-line arguments:");
    for(i=optind; i < argc; i++) fprintf(stderr, " %s", argv[i]);
    fprintf(stderr, "\n");
    res = RES_BAD_ARG;
    goto error;
  }

  /* Set some computation-related stuff */
  system->offset = system->radius * cos(system->contact_angle * PI / 180);
  system->compute_range[0] = cos(system->contact_angle * PI / 180);
  system->compute_range[1] = provided_I ? max_range : system->radius;

  /* Check consistency */
  if(provided_S && !provided_i) {
    fprintf(stderr, "Cannot use -S option without -i option.\n");
    res = RES_BAD_ARG;
    goto error;
  }
  if(provided_I &&
      (max_range <= system->offset || max_range > system->radius))
  {
    fprintf(stderr, "Invalid argument for option I: %g.\n", max_range);
    fprintf(stderr, "Valid range is ]%.9g %.9g].\n",
      system->offset, system->radius);
    res = RES_BAD_ARG;
    goto error;
  }
  if(provided_P &&
      (system->args_probe[2] <= 0
       || system->args_probe[2] > system->radius - system->offset))
  {
    fprintf(stderr, "Invalid argument for option P: %g.\n",
      system->args_probe[2]);
    fprintf(stderr, "Valid range is ]0 %.9g].\n",
      system->radius - system->offset);
    res = RES_BAD_ARG;
    goto error;
  }
  if(provided_A && system->ref_t) {
    fprintf(stderr, "Cannot use -r option in analytic mode.\n");
    res = RES_BAD_ARG;
    goto error;
  }
  if(provided_A && system->ref_f) {
    fprintf(stderr, "Cannot use -R option in analytic mode.\n");
    res = RES_BAD_ARG;
    goto error;
  }

end:
  if(help) short_help(stderr);
  return res;
error:
  help = 1;
  goto end;
}
