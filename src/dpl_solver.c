/* Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "droplet.h"
#include "dpl_solver.h"
#include "dpl_walk.h"
#include "dpl_system.h"

#include <star/ssp.h>
#include <rsys/double3.h>

#include <omp.h>
#include <stddef.h>

void
sample_probe
  (struct ssp_rng* rng,
   struct system* system,
   double probe[3],
   double* pdf)
{
  ASSERT(system && probe && pdf);

  switch(system->sampling_mode ) {
    case SAMPLE_UNIFORM: {
      double cos_range[2];
      cos_range[0] = system->compute_range[0] / system->radius;
      cos_range[1] = system->compute_range[1] / system->radius;
      ssp_ran_spherical_zone_uniform_local(rng, cos_range, probe, pdf);
      d3_muld(probe, probe, system->radius);

      break;
    }

    default: FATAL("Invalid type.\n");
  }
  ASSERT(localize(probe, system, NULL) == DOMAIN_H_BOUNDARY);
}

res_T
solve
  (struct system* system,
   struct estimator* result)
{
  ASSERT(system && result);
  if(system->compute_mode == COMPUTE_PROBE)
    return probe_solve(system->solver_probe, system, result);
  else return mean_solve(system, result);
}

res_T
mean_solve
  (struct system* system,
   struct estimator* result)
{
  res_T res = RES_OK;
  struct ssp_rng_proxy* proxy = NULL;
  struct ssp_rng** rngs = NULL;
  struct accum* acc_temp_s = NULL;
  struct accum* acc_flux_s = NULL;
  struct accum* acc_steps_s = NULL;
  struct accum* acc_reinjections_s = NULL;
  struct accum* acc_air_vs_base_s = NULL;
  int64_t c;
  double alpha, surf;
  unsigned i;

  ASSERT(system && result);

  /* Allocate per-thread random generators */
  ERR(ssp_rng_proxy_create(&system->allocator, &ssp_rng_threefry,
    system->nthreads, &proxy));
  rngs = MEM_CALLOC(&system->allocator, system->nthreads, sizeof(*rngs));
  if(!rngs) {
    res = RES_MEM_ERR;
    goto error;
  }
  for(i = 0; i < system->nthreads; i++)
    ERR(ssp_rng_proxy_create_rng(proxy, i, rngs+i));

  /* Allocate per-thread accumulators */
  ALLOC_ACCUMS(acc_temp_s);
  ALLOC_ACCUMS(acc_flux_s);
  ALLOC_ACCUMS(acc_steps_s);
  ALLOC_ACCUMS(acc_reinjections_s);
  ALLOC_ACCUMS(acc_air_vs_base_s);

  alpha = system->contact_angle * PI / 180;
  surf = 2 * PI * SQR(system->radius) * (1 - cos(alpha));

  /* Launch the MC simulation */
  omp_set_num_threads((int)system->nthreads);
  #pragma omp parallel for schedule (static)
  for(c = 0; c < (int64_t)system->sample_count; c++) {
    const int ithread = omp_get_thread_num();
    struct ssp_rng* rng = rngs[ithread];
    struct accum* acc_temp = acc_temp_s + ithread;
    struct accum* acc_flux = acc_flux_s + ithread;
    struct accum* acc_steps = acc_steps_s + ithread;
    struct accum* acc_reinjections = acc_reinjections_s + ithread;
    struct accum* acc_air_vs_base = acc_air_vs_base_s + ithread;
    struct sample sample = SAMPLE_DEFAULT;
    double probe[3], pdf, wt, wf;
    res_T tmp_res;

    sample_probe(rng, system, probe, &pdf);

    ASSERT(localize(probe, system, NULL) == DOMAIN_H_BOUNDARY);
    tmp_res = walk(probe, rng, system, &sample);
    if(tmp_res != RES_OK) {
      continue;
    }

    wt = sample.temp / (pdf * surf);
    wf = sample.flux / pdf;

    /* Accumulate samples */
    ACCUM_ADD(*acc_temp, wt);
    ACCUM_ADD(*acc_flux, wf);
    ACCUM_ADD(*acc_steps, (double)sample.steps);
    ACCUM_ADD(*acc_reinjections, (double)sample.reinjections);
    ACCUM_ADD(*acc_air_vs_base, (double)sample.air_vs_base);
  }
  if(res != RES_OK) goto error;

  /* Reduction of the per-thread accumulators */
  ACCUM_REDUX(acc_temp_s, system->nthreads);
  ACCUM_REDUX(acc_flux_s, system->nthreads);
  ACCUM_REDUX(acc_steps_s, system->nthreads);
  ACCUM_REDUX(acc_reinjections_s, system->nthreads);
  ACCUM_REDUX(acc_air_vs_base_s, system->nthreads);

  /* Create result */
  result->temperature = acc_temp_s[0];
  result->flux = acc_flux_s[0];
  result->steps = acc_steps_s[0];
  result->reinjections = acc_reinjections_s[0];
  result->air_vs_base = acc_air_vs_base_s[0];
  result->n_realisations = system->sample_count;
  result->n_failures = system->sample_count - result->temperature.count;

end:
  /* Release local objects */
  if(rngs) {
    for(i = 0; i < system->nthreads; i++)
      if(rngs[i]) SSP(rng_ref_put(rngs[i]));
    MEM_RM(&system->allocator, rngs);
  }
  if(proxy) SSP(rng_proxy_ref_put(proxy));
  if(acc_temp_s) MEM_RM(&system->allocator, acc_temp_s);
  if(acc_flux_s) MEM_RM(&system->allocator, acc_flux_s);
  if(acc_steps_s) MEM_RM(&system->allocator, acc_steps_s);
  if(acc_reinjections_s) MEM_RM(&system->allocator, acc_reinjections_s);
  if(acc_air_vs_base_s) MEM_RM(&system->allocator, acc_air_vs_base_s);

  return res;
error:
  goto end;
}

res_T
probe_solve
  (const double probe[3],
   struct system* system,
   struct estimator* result)
{
  res_T res = RES_OK;
  struct ssp_rng_proxy* proxy = NULL;
  struct ssp_rng** rngs = NULL;
  struct accum* acc_temp_s = NULL;
  struct accum* acc_flux_s = NULL;
  struct accum* acc_steps_s = NULL;
  struct accum* acc_reinjections_s = NULL;
  struct accum* acc_air_vs_base_s = NULL;
  int64_t r;
  unsigned i;

  ASSERT(probe && system && result);

  /* Allocate per-thread random generators */
  ERR(ssp_rng_proxy_create(&system->allocator, &ssp_rng_threefry,
    system->nthreads, &proxy));
  rngs = MEM_CALLOC(&system->allocator, system->nthreads, sizeof(*rngs));
  if(!rngs) {
    res = RES_MEM_ERR;
    goto error;
  }
  for(i = 0; i < system->nthreads; i++)
    ERR(ssp_rng_proxy_create_rng(proxy, i, rngs+i));

  /* Allocate per-thread accumulators */
  ALLOC_ACCUMS(acc_temp_s);
  ALLOC_ACCUMS(acc_flux_s);
  ALLOC_ACCUMS(acc_steps_s);
  ALLOC_ACCUMS(acc_reinjections_s);
  ALLOC_ACCUMS(acc_air_vs_base_s);

  /* Launch the MC simulation */
  omp_set_num_threads((int)system->nthreads);
  #pragma omp parallel for schedule (static)
  for(r = 0; r < (int64_t)system->sample_count; r++) {
    const int ithread = omp_get_thread_num();
    struct ssp_rng* rng = rngs[ithread];
    struct accum* acc_temp = acc_temp_s + ithread;
    struct accum* acc_flux = acc_flux_s + ithread;
    struct accum* acc_steps = acc_steps_s + ithread;
    struct accum* acc_reinjections = acc_reinjections_s + ithread;
    struct accum* acc_air_vs_base = acc_air_vs_base_s + ithread;
    struct sample sample = SAMPLE_DEFAULT;
    res_T tmp_res;

    tmp_res = walk(probe, rng, system, &sample);
    if(tmp_res != RES_OK) {
      continue;
    }

    /* Accumulate samples */
    ACCUM_ADD(*acc_temp, sample.temp);
    ACCUM_ADD(*acc_flux, sample.flux);
    ACCUM_ADD(*acc_steps, (double)sample.steps);
    ACCUM_ADD(*acc_reinjections, (double)sample.reinjections);
    ACCUM_ADD(*acc_air_vs_base, (double)sample.air_vs_base);
  }
  if(res != RES_OK) goto error;

  /* Reduction of the per-thread accumulators */
  ACCUM_REDUX(acc_temp_s, system->nthreads);
  ACCUM_REDUX(acc_flux_s, system->nthreads);
  ACCUM_REDUX(acc_steps_s, system->nthreads);
  ACCUM_REDUX(acc_reinjections_s, system->nthreads);
  ACCUM_REDUX(acc_air_vs_base_s, system->nthreads);

  /* Create result */
  result->temperature = acc_temp_s[0];
  result->flux = acc_flux_s[0];
  result->steps = acc_steps_s[0];
  result->reinjections = acc_reinjections_s[0];
  result->air_vs_base = acc_air_vs_base_s[0];
  result->n_realisations = system->sample_count;
  result->n_failures = system->sample_count - result->temperature.count;

end:
  /* Release local objects */
  if(rngs) {
    for(i = 0; i < system->nthreads; i++)
      if(rngs[i]) SSP(rng_ref_put(rngs[i]));
    MEM_RM(&system->allocator, rngs);
  }
  if(proxy) SSP(rng_proxy_ref_put(proxy));
  if(acc_temp_s) MEM_RM(&system->allocator, acc_temp_s);
  if(acc_flux_s) MEM_RM(&system->allocator, acc_flux_s);
  if(acc_steps_s) MEM_RM(&system->allocator, acc_steps_s);
  if(acc_reinjections_s) MEM_RM(&system->allocator, acc_reinjections_s);
  if(acc_air_vs_base_s) MEM_RM(&system->allocator, acc_air_vs_base_s);

  return res;
error:
  goto end;
}
