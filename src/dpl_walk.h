/* Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef DROPLET_WALK_H
#define DROPLET_WALK_H

#include "dpl_system.h"

#include <rsys/rsys.h>

#define IS_INSIDE(Pos) (DOMAIN_OUTSIDE != localize((Pos), system, NULL))

/* Different domains of the system */
enum domain {
  DOMAIN_BASE = BIT(0),
  DOMAIN_H_BOUNDARY = BIT(1),
  DOMAIN_DROPLET = BIT(2),
  DOMAIN_OUTSIDE = BIT(3),
  DOMAIN_UNDEFINED = BIT(4),

  DOMAIN_BORDER = DOMAIN_BASE | DOMAIN_H_BOUNDARY
};

/* hit type used for closest geometry queries */
struct hit {
  double dir[3];      /* Direction of the closest geometry */
  double dist;        /* Distance of the geometry along dir */
  enum domain domain; /* The geometry that was hit */
};

/* step type used for the current step of the walk */
struct step {
  double pos[3];       /* Current position */
  size_t steps;        /* Number of steps since walk started */
  size_t reinjections; /* Number of reinjections since walk started */
  size_t air_vs_base;  /* Number of samplings air VS base */
  enum domain domain; /* The domain the step is in */
};
#define STEP_DEFAULT { {0,0}, 0, 0, 0, DOMAIN_UNDEFINED }

/* Type for samples results */
struct sample {
  double temp;
  double flux;
  size_t steps;
  size_t reinjections;
  size_t air_vs_base;
};
#define SAMPLE_DEFAULT { 0, 0, 0, 0, 0 }

/* Type for the computed temperature */
struct temp {
  double T;
  int known;
};
#define TEMP_DEFAULT { 0, 0 }

extern LOCAL_SYM enum domain
localize
  (const double pos[3],
   const struct system* system,
   enum domain* if_forced); /* Can be NULL */

extern LOCAL_SYM double
dist_to_base
  (const double pos[3],
   const struct system* system);

extern LOCAL_SYM void
closest_geometry
  (const double pos[3],
   const struct system* system,
   struct hit* hit);

extern LOCAL_SYM double
pdist_to_sphere
  (const double pos[3],
   const double dir[3],
   const double r);

extern LOCAL_SYM void
geometry_distance
  (const double pos[3],
   const double dir[3],
   const enum domain from,
   const struct system* system,
   struct hit* hit);

extern LOCAL_SYM res_T
one_conductive_step
  (struct step* step,
   struct ssp_rng* rng,
   const struct system* system);

extern LOCAL_SYM res_T
h_boundary_reinjection
  (struct step* step,
   struct ssp_rng* rng,
   const struct system* system,
   struct temp* temp);

extern LOCAL_SYM res_T
walk
  (const double pos[3],
   struct ssp_rng* rng,
   const struct system* system,
   struct sample* result);

#endif
