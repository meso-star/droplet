/* Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef DROPLET_SYSTEM_H
#define DROPLET_SYSTEM_H

#include <rsys/rsys.h>
#include <rsys/mem_allocator.h>

/* Forward declarations */
struct ssp_rng;
struct logger;

/*
 * The system is made of a planar base and a sphere.
 * The base is on the X axis at Z=offset and intersects the sphere.
 * In computations the center of the sphere is at the origin.
 *
 * As seen by the user through command line options, the base is at Z=0 and the
 * sphere's center is at Z=-offset (simple Z offset).
 *
 *                ^
 *              Z |
 *                |
 *       Tair   o | o
 *      hc   o    |    o
 *         o      |   /  o
 *     \  o       | /R    o
 *      \o        *--------o---->
 *       \o angle         o   X
 *________\o______+______o__________
 *          Tbase
 *
 *                ^
 *              Z |
 *           /    |
 *          /   o | o   Tair
 *         / o angle   o  hc
 *________/o______+___/__o__________
 *       Tbase    | /R
 *                *------------->
 *                            X
 *
 * When in analytic mode, the systems has a few more parameters that
 * help describe the analytic fluid temperature in a new coordinate
 * system:
 *
 * In this new system: Xc = R+X1 (X1 > 0), Yc = -cos(angle).
 *                 
 *                 
 *     ^           
 *     |        o   o P 
 *     |     o      /  o
 *     |   o       /     o
 *     |  o       / theta o
 *     | o      c *--------o     
 *     |  o     /   \     o    
 *     |   o   /     \   o
 *     |_____o/_______\o____>
 *    O       theta2   theta1 
 *
 */

enum compute_mode {
  COMPUTE_MEANS,
  COMPUTE_PROBE
};

enum probe_location {
  PROBE_ON_BOUNDARY,
  PROBE_IN_DROPLET
};

enum solver_mode {
  STANDARD_MODE = BIT(0),
  ANALYTIC_SPHERE = BIT(1)
};

enum sampling_mode {
  SAMPLE_UNIFORM
};

/* The type that describes the system */
struct system {
  double radius;   /* The radius of the truncated sphere */
  double contact_angle; /* Angle betwwen base and sphere normal (in degrees) */
  double offset;   /* The offset of base on axis Z; in ]-R +R[ */

  double Tair, hc; /* Boundary with air along the sphere */
  double Tbase;    /* Imposed temperature at the base */
  double lambda;   /* Conductivity of the fluid */
  
  double boundary_epsilon;     /* Boundary thickness relative to radius */
  double reinjection_length;   /* Length of reinjection steps, in 
                                  boundary_epsilon */

  unsigned nthreads;           /* Number of threads used */
  unsigned long sample_count;  /* Number of samples */

  struct mem_allocator allocator; /* Memory allocator */
  int alloc_initialized;          /* Flag */

  /* Computation-related stuff */
  double args_probe[3], solver_probe[3], user_probe[3];
  double x1;
  double compute_range[2]; /* Zone to compute if -i */
  double reference_temperature, reference_flux;
  enum solver_mode solver_mode;
  enum compute_mode compute_mode;
  enum probe_location probe_location;
  enum sampling_mode sampling_mode;
  int compact_result, ref_t, ref_f;
};

extern LOCAL_SYM res_T
init_system
  (struct system* system);

extern LOCAL_SYM void
clear_system
  (struct system* system);

extern LOCAL_SYM res_T
set_system_physics
  (double Tair,
   double Tbase,
   double lambda,
   struct system* system);

extern LOCAL_SYM res_T
define_probe
  (struct system* system);

/* T of the fluid surrounding the droplet */
extern LOCAL_SYM double
fluid_T
  (const struct system* system,
   const double pos[3]);

/* T at any pos in the analytic system */
extern LOCAL_SYM double
analytic_T
  (const struct system* system,
   const double pos[3]);

extern LOCAL_SYM double
fluid_proba
  (double delta,
   const struct system* system);

#endif

