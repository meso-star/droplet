/* Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "droplet.h"
#include "dpl_system.h"
#include "dpl_num_params.h"
#include "dpl_walk.h"

#include <rsys/rsys.h>
#include <rsys/double3.h>
#include <rsys/mem_allocator.h>

#include <omp.h>
#include <stdio.h>

/****************************/
/* Initialization functions */
/****************************/
res_T
init_system
  (struct system* system)
{
  res_T res = RES_OK;

  ASSERT(system);

  /* Set default values */
  system->boundary_epsilon = BOUNDARY_EPSILON;
  system->reinjection_length = REINJECTION_LENGTH;
  system->nthreads = (unsigned)omp_get_num_procs();
  system->radius = 1;
  system->solver_mode = STANDARD_MODE;
  system->sampling_mode = SAMPLE_UNIFORM;
  system->compact_result = 0;
  system->ref_t = 0;
  system->ref_f = 0;
  system->sample_count = 10000;
  d3(system->args_probe, INF, INF, INF);
  d3(system->solver_probe, INF, INF, INF);
  d3(system->user_probe, INF, INF, INF);

  /* Create memory allocator */
  ERR(mem_init_proxy_allocator(&system->allocator, &mem_default_allocator));
  system->alloc_initialized = 1;

end:
  return res;

error:
  clear_system(system);
  goto end;
}

void
clear_system
  (struct system* system)
{
  ASSERT(system);

  if(system->alloc_initialized) {
    if(MEM_ALLOCATED_SIZE(&system->allocator) != 0) {
      char dump[4096] = { '\0' };
      MEM_DUMP(&system->allocator, dump, sizeof(dump));
      fprintf(stderr, "%s\n", dump);
      fprintf(stderr, "\nMemory leaks: %lu Bytes.\n",
        (unsigned long)MEM_ALLOCATED_SIZE(&system->allocator));
    }
    system->alloc_initialized = 0;
  }
}

res_T
set_system_physics
  (double Tair,
   double Tbase,
   double lambda,
   struct system* system)
{
  res_T res = RES_OK;

  ASSERT(Tair >= 0 && Tbase >= 0 && lambda != 0 && system);

  system->Tair = Tair;
  system->Tbase = Tbase;
  system->lambda = lambda;

  return res;
}

res_T
define_probe
  (struct system* system)
{
  res_T res = RES_OK;
  enum domain loc;

  ASSERT(system);

  switch(system->probe_location) {
    case PROBE_ON_BOUNDARY:
      system->solver_probe[2] = system->args_probe[2] + system->offset;
      system->solver_probe[0] =
        sqrt(SQR(system->radius) - SQR(system->solver_probe[2]));
      system->solver_probe[1] = 0;
      if(system->solver_mode & ANALYTIC_SPHERE)
        system->solver_probe[0] *= -1;
      break;
    case PROBE_IN_DROPLET:
      system->solver_probe[2] = system->args_probe[2] + system->offset;
      system->solver_probe[0] = system->args_probe[0];
      system->solver_probe[1] = system->args_probe[1];
      break;
    default:
      FATAL("Type error.\n");
  }
  system->user_probe[0] = system->solver_probe[0];
  system->user_probe[1] = system->solver_probe[1];
  system->user_probe[2] = system->args_probe[2];

  /* Check probe validity */
  loc = localize(system->solver_probe, system, NULL);
  if(system->probe_location == PROBE_IN_DROPLET && loc != DOMAIN_DROPLET) {
    fprintf(stderr, "Probe is not in the droplet: %.9g,%.9g.\n",
      SPLIT2(system->args_probe));
    res = RES_BAD_ARG;
    goto error;
  }
  if(system->probe_location == PROBE_ON_BOUNDARY
    && !(loc & DOMAIN_H_BOUNDARY))
  {
    fprintf(stderr, "Probe is not on the boundary: %.9g.\n",
      system->args_probe[1]);
    res = RES_BAD_ARG;
    goto error;
  }

end:
  return res;
error:
  goto end;
}

double
fluid_T
  (const struct system* system,
   const double pos[3])
{
  double H, L, TH, Tinf, Tb, X1;
  double Tf, Ta;
  double x, y; /* Coordinates in the analytic system */

  ASSERT(system && pos);

  Tinf = system->Tair;

  if(!(system->solver_mode & ANALYTIC_SPHERE))
    return Tinf;

  X1 = system->x1;
  TH = PI / 2;
  H = system->hc;
  L = system->lambda;
  Tb = system->Tbase;

  Ta = analytic_T(system, pos);

  x = pos[0] + X1 + system->radius;
  y = pos[1] - system->offset;

  /* Check that pos is at the h boundary */
  ASSERT(eq_eps(system->radius, d3_len(pos), 1e-6));

  Tf = Ta + (L / H) * (Tinf - Tb) / (TH * system->radius)
   * (x * pos[1] - y * pos[0]) / (SQR(x) + SQR(y));

  return Tf;
}

double
analytic_T
  (const struct system* system,
   const double pos[3])
{
  double TH, Tinf, Tb, X1;
  double Ta;
  double x, y;

  ASSERT(system && pos);
  ASSERT(system->solver_mode & ANALYTIC_SPHERE);

  X1 = system->x1;
  TH = PI / 2;
  Tinf = system->Tair;
  Tb = system->Tbase;

  /* Coordinates in the analytic system */
  x = pos[0] + X1 + system->radius;
  y = pos[1] - system->offset;

  Ta = (Tinf - Tb) / TH * atan2(y, x) + Tb;

  ASSERT(MMIN(Tb, Tinf) <= Ta && Ta <= MMAX(Tb, Tinf));
  return Ta;
}

double
fluid_proba
  (double delta,
   const struct system* system)
{
  double hc;

  ASSERT(delta > 0 && system);

  hc = system->hc;
  if(!(hc < (hc + system->lambda / delta)))
    /* Mostly to handle hc=INF */
    return 1;
  return hc / (hc + system->lambda / delta);
}
