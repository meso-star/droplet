/* Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef DROPLET_SOLVER_H
#define DROPLET_SOLVER_H

#include <rsys/rsys.h>

#include <math.h>

/* Forward declarations */
struct system;
struct ssp_rng;

/* Type for MC accumulators */
struct accum {
  double sum;
  double sum2;
  size_t count;
};
#define ACCUM_DEFAULT { 0, 0, 0 }

#define ALLOC_ACCUMS(Dst) { \
  (Dst) = MEM_CALLOC(&system->allocator, system->nthreads, sizeof(*(Dst))); \
  if(!(Dst)) { res = RES_MEM_ERR;  goto error; } \
} (void)0

#define ACCUM_ADD(A, V) { \
  (A).sum += (V); (A).sum2 += SQR(V); (A).count++; \
}

#define ACCUM_REDUX(AccArray, Count) { \
  size_t i___; \
  for(i___ = 1; i___ < (Count); i___++) { \
    (AccArray)[0].sum += (AccArray)[i___].sum; \
    (AccArray)[0].sum2 += (AccArray)[i___].sum2; \
    (AccArray)[0].count += (AccArray)[i___].count; \
}}

/* Type for MC simulation results */
struct estimator {
  struct accum temperature;
  struct accum flux;
  struct accum steps;
  struct accum reinjections;
  struct accum air_vs_base;

  size_t n_realisations;
  size_t n_failures;
};
#define ESTIMATOR_DEFAULT \
  { \
    ACCUM_DEFAULT, ACCUM_DEFAULT, ACCUM_DEFAULT, ACCUM_DEFAULT, ACCUM_DEFAULT, \
    0, 0 \
  }

/* Type for MC estimates */
struct mc {
  double E;
  double V;
  double SE;
};
#define MC_DEFAULT { 0, 0, 0 }

#define SETUP_MC(Mc, Acc) { \
  (Mc)->E = (Acc)->sum / (double)(Acc)->count; \
  (Mc)->V = MMAX(0, (Acc)->sum2 / (double)(Acc)->count -(Mc)->E * (Mc)->E); \
  (Mc)->SE = sqrt((Mc)->V / (double)(Acc)->count); \
}

#define SPLITMC(Mc) (Mc).E, (Mc).SE

extern LOCAL_SYM void
sample_probe
  (struct ssp_rng* rng,
   struct system* system,
   double probe[3],
   double* pdf);

extern LOCAL_SYM res_T
solve
  (struct system* system,
   struct estimator* result);

extern LOCAL_SYM res_T
mean_solve
  (struct system* system,
   struct estimator* result);

extern LOCAL_SYM res_T
probe_solve
  (const double probe[3],
   struct system* system,
   struct estimator* result);

#endif
