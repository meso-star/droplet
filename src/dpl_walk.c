/* Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "droplet.h"
#include "dpl_system.h"
#include "dpl_walk.h"

#include <rsys/rsys.h>
#include <rsys/double2.h>
#include <rsys/double3.h>
#include <star/ssp.h>

#include <math.h>

enum domain
localize
  (const double pos[3],
   const struct system* system,
   enum domain* if_forced) /* Can be NULL */
{
  double epsilon;
  struct hit hit;

  ASSERT(pos && system);

  /* Find closest_geometry() */
  closest_geometry(pos, system, &hit);
  
  epsilon = system->boundary_epsilon * system->radius;
  if(hit.dist <= epsilon) {
    /* Close enough to be at some border */
    ASSERT(hit.domain & DOMAIN_BORDER);
    return hit.domain;
  }

  /* Check against base */
  if(hit.domain == DOMAIN_BASE && pos[2] < system->offset) {
    if(if_forced) *if_forced = DOMAIN_BASE;
    return DOMAIN_OUTSIDE;
  }
  /* Check against sphere */
  if(system->radius < d3_len(pos)) {
    if(if_forced) *if_forced = DOMAIN_H_BOUNDARY;
    return DOMAIN_OUTSIDE;
  }

  return DOMAIN_DROPLET;
}

double
dist_to_base
  (const double pos[3],
   const struct system* system)
{
  /* x_off is X of the base/disk contact point in a 2D projection */
  double x_off = sqrt(SQR(system->radius) - SQR(system->offset));
  double dx = fabs(pos[0]) - x_off;
  double dy = pos[2] - system->offset;

  ASSERT(pos && system);

  if(dx <= 0)
    /* Closest point on base is just below pos */
    return dy;
  else
    /* Closest point is x_off/offset */
    return sqrt(SQR(dx) + SQR(dy));
}

void
closest_geometry
  (const double pos[3],
   const struct system* system,
   struct hit* hit)
{
  double bd, gd;
  enum domain geom_domain;
  double dir[3];

  ASSERT(pos && system && hit);

  /* Distance of the base */
  bd = fabs(pos[2] - system->offset);
  /* Distance of the sphere */
  gd = fabs(system->radius - d3_len(pos));
  geom_domain = DOMAIN_H_BOUNDARY;
  d3_set(dir, pos);

  if(gd < bd) {
    hit->dist = gd;
    d3_normalize(hit->dir, dir);
    hit->domain = geom_domain;
  } else {
    hit->dist = bd;
    d3(hit->dir, 0, 0, -1);
    hit->domain = DOMAIN_BASE;
  }
}

double
pdist_to_sphere
  (const double pos[3],
   const double dir[3],
   const double r)
{
  double a, b, c, d, rd, r1, r2;
  double tmp[3]; (void)tmp;

  ASSERT(pos && dir && r > 0);

  a = SQR(dir[0]) + SQR(dir[1]) + SQR(dir[2]);
  b = 2 * (pos[0] * dir[0] + pos[1] * dir[1] + pos[2] * dir[2]);
  c = SQR(pos[0]) + SQR(pos[1]) + SQR(pos[2]) - SQR(r);

  if(a == 0) {
    if(b == 0)
      return (c == 0) ? 0 : INF;
    return -c / b;
  }

  d = SQR(b) - 4 * a * c;

  if(d < 0)
    return INF;

  if(d == 0) {
    r1 = 0.5 * -b / a;
    ASSERT(eq_eps(r,
      d3_len(d3_add(tmp, pos, (d3_muld(tmp, dir, r1)))),
      r * 1e-8));
    r1 = (r1 < 0) ? INF : r1;
    return r1;
  }

  rd = sqrt(d);
  r1 = 0.5 * (-b + rd) / a;
  ASSERT(eq_eps(r,
    d3_len(d3_add(tmp, pos, (d3_muld(tmp, dir, r1)))),
    r * 1e-8));
  r1 = (r1 < 0) ? INF : r1;
  r2 = 0.5 * (-b - rd) / a;
  ASSERT(eq_eps(r,
    d3_len(d3_add(tmp, pos, (d3_muld(tmp, dir, r2)))),
    r * 1e-8));
  r2 = (r2 < 0) ? INF : r2;
  return MMIN(r1, r2);
}

void
geometry_distance
  (const double pos[3],
   const double dir[3],
   const enum domain from,
   const struct system* system,
   struct hit* hit)
{
  double bd, gd;
  enum domain geom_domain;

  ASSERT(pos && dir && system && hit && d3_is_normalized(dir));
  
  /* Distance to the base */
  if(from == DOMAIN_BASE)
    bd = INF;
  else if(dir[2] == 0) {
    bd = (pos[2] == system->offset) ? 0 : INF;
  } else {
    bd = -(pos[2] - system->offset) / dir[2];
    if(bd < 0) bd = INF;
  }
  /* Distance to the sphere */
  if(from == DOMAIN_H_BOUNDARY) {
    gd = INF;
    geom_domain = DOMAIN_UNDEFINED;
  } else {
    gd = pdist_to_sphere(pos, dir, system->radius);
    geom_domain = DOMAIN_H_BOUNDARY;
   }

  if(gd < bd) {
    hit->dist = gd;
    hit->domain = geom_domain;
  } else {
    hit->dist = bd;
    hit->domain = (bd != INF) ? DOMAIN_BASE : DOMAIN_UNDEFINED;
  }
  d3_set(hit->dir, dir);
}

res_T
one_conductive_step
  (struct step* step,
   struct ssp_rng* rng,
   const struct system* system)
{
  res_T res = RES_OK;
  struct hit hit;
  double tmp[3];
  double delta, epsilon;
  int to_boundary = 0;

  ASSERT(step && rng && system && step->domain == DOMAIN_DROPLET);
  ASSERT(IS_INSIDE(step->pos));

  /* Compute geometry distance */
  closest_geometry(step->pos, system, &hit);

  /* As it is a steady computation, step lenght has not to be limited */
  delta = hit.dist;

  /* Check if at boundary */
  epsilon = system->boundary_epsilon * system->radius;
  if(hit.dist < epsilon) {
    /* Step go to boundary */
    to_boundary = 1;
    step->domain = hit.domain;
    /* Move */
    d3_add(step->pos, step->pos, d3_muld(tmp, hit.dir, delta));
  } else {
    double move[3];
    ssp_ran_sphere_uniform(rng, move, NULL);
    d3_add(step->pos, step->pos, d3_muld(move, move, delta));
  }

  /* Fix position if needed */
  if(!to_boundary) {
    enum domain forced = DOMAIN_UNDEFINED ;
    step->domain = localize(step->pos, system, &forced);
    if(!IS_INSIDE(step->pos)) {
      step->domain = forced;
      if(forced == DOMAIN_BASE)
        step->pos[2] = system->offset;
      else {
        ASSERT(forced == DOMAIN_H_BOUNDARY);
        step->pos[2] = sqrt(SQR(system->radius) - SQR(step->pos[0]));
      }
    }
  }

  step->steps++;
  return res;
}

res_T
h_boundary_reinjection
  (struct step* step,
   struct ssp_rng* rng,
   const struct system* system,
   struct temp* temp)
{
  res_T res = RES_OK;
  double r, p_air;
  double epsilon, delta;
  double norm, n[3] = { 0, 0 };
  int step_to_base = 0;
  struct hit hit;

  ASSERT(step && rng && system && temp && step->domain == DOMAIN_H_BOUNDARY);

  /* Compute delta, length of a possible step back into the droplet */
  norm = d3_normalize(n, step->pos);
  ASSERT(norm); (void)norm;
  d3_minus(n, n);

  /* Check if geometry distance allows this step */
  geometry_distance(step->pos, n, DOMAIN_H_BOUNDARY, system, &hit);

  epsilon = system->boundary_epsilon * system->radius;
  if(hit.dist <= 2 * epsilon) {
    /* Step directly to the base */
    delta = hit.dist;
    step_to_base = 1;
    step->air_vs_base++;
  } else {
    /* If close to the geometry, temperature gradient can be huge
     * and reinjection distance must be reduced */
    double bd = dist_to_base(step->pos, system) / system->radius;
    double rl = system->reinjection_length;
    double threshold = sqrt(system->boundary_epsilon * rl);
    /* At bd=0, reinject at 1, at bd=threshold reinject at rl */
    double reinjection = MMIN(rl, 1 + (rl - 1) / SQR(threshold) * SQR(bd));
    delta = MMIN(reinjection * epsilon, hit.dist * 0.5);
  }
  ASSERT(delta > 0);

  /* Compute the probability to go into the air */
  p_air = fluid_proba(delta, system);

  /* Chose between the 2 sides */
  r = ssp_rng_canonical(rng);
  if(r < p_air) {
    /* Go to air, where temperature is known */
    temp->known = 1;
    temp->T =  fluid_T(system, step->pos);
  } else if(step_to_base) {
    /* Jump to base, where temperature is known */
    temp->known = 1;
    temp->T = system->Tbase;
  } else {
    /* Back to the droplet */
    double tmp[3];
    /* Move into the droplet */
    d3_add(step->pos, step->pos, d3_muld(tmp, n, delta));
    step->reinjections++;
    step->domain = DOMAIN_DROPLET;
  }
  ASSERT(IS_INSIDE(step->pos));

  return res;
}

res_T
walk
  (const double pos[3],
   struct ssp_rng* rng,
   const struct system* system,
   struct sample* result)
{
  res_T res = RES_OK;
  double Tf = -1;
  struct step step = STEP_DEFAULT;
  struct temp temp = TEMP_DEFAULT;

  ASSERT(pos && rng && system && result && IS_INSIDE(pos));

  if(system->probe_location == PROBE_ON_BOUNDARY)
    Tf = fluid_T(system, pos);

  /* Initialize step */
  d3_set(step.pos, pos);
  step.domain = localize(pos, system, NULL);

  /* walk until known temperature */
  while(!temp.known) {
    switch(step.domain) {
      case DOMAIN_BASE:
        temp.known = 1;
        temp.T = system->Tbase;
        break;
      case DOMAIN_DROPLET:
        ERR(one_conductive_step(&step, rng, system));
        break;
      case DOMAIN_H_BOUNDARY:
        ERR(h_boundary_reinjection(&step, rng, system, &temp));
        break;
      case DOMAIN_OUTSIDE:
        /* Should not be there! */
        res = RES_BAD_OP;
        goto error;
      default:
        FATAL("Type error.\n");
    }
  }

  /* Build result */
  result->temp = temp.T;
  if(system->probe_location == PROBE_ON_BOUNDARY)
    result->flux = (Tf - result->temp) * system->hc;
  result->steps = step.steps;
  result->reinjections = step.reinjections;
  result->air_vs_base = step.air_vs_base;

end:
  return res;

error:
  goto end;
}
